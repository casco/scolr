initialize-release
initializeSubcomponents
	setSelectorSubcomponent := SetSelectorSubComponent parent: self.
	articleListSubComponent := ArticleListSubComponent parent: self.
	reportSubcomponent := ReportSubcomponent parent: self.
	classifyArticlesListSubcomponent := ClassifyArticlesListSubcomponent parent: self.
	reviewArticlesListSubcomponent := ReviewArticlesListSubcomponent parent: self.
	reviewSummarySubComponent := ReviewSummarySubComponent parent: self.
	discussSubcomponent := DiscussSubcomponent parent: self.