callbacks
edit
	notes := self coReview notes.
	inclusionCriteria := self coReview inclusionCriteria.
	exclusionCriteria := self coReview exclusionCriteria.
	title := self coReview title.
	isEditing := true